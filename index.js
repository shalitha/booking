var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000,
  mongoose = require('mongoose'),
  // Task = require('./api/models/todoList'), //created model loading here
  bodyParser = require('body-parser');
  const pug = require('pug');

// mongoose instance connection url connection
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/todoList');


var MongoClient = require('mongodb').MongoClient

// MongoClient.connect('mongodb://localhost/booking', function (err, db) {
//   if (err) throw err;
//
//   db.collection('booking').find().toArray(function (err, result) {
//     if (err) throw err
//
//     console.log(result)
//   });
// });

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.set('view engine', 'pug');


// var routes = require('./api/routes/todoListRoutes'); //importing route
// routes(app); //register the route

app.get('/', function(req, res){
  res.render('index', { title: 'Hey', message: 'Hello there!' });
});


app.listen(port);


console.log('todo list RESTful API server started on: ' + port);
